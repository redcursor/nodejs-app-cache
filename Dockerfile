from alpine:latest
arg BUILDKIT_INLINE_CACHE=1
workdir /nodejs/

copy ./index.js ./
copy ./log.txt  ./
# copy ./node_modules ./

cmd node index.js
